#include "fft.hh"
#include "my_types.hh"
#include <gtest/gtest.h>

#include "compute_temperature.hh"
#include "material_points_factory.hh"
#include "particle.hh"
#include "system.hh"
#include <array>
#include <random>

/*****************************************************************/
TEST(FFT, transform) {
  UInt N = 512;
  Matrix<complex> m(N);

  Real k = 2 * M_PI / N;
  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    val = cos(k * i);
  }

  Matrix<complex> res = FFT::transform(m);

  for (auto&& entry : index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    if (std::abs(val) > 1e-10)
      std::cout << i << "," << j << " = " << val << std::endl;

    if (i == 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else if (i == N - 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else
      ASSERT_NEAR(std::abs(val), 0, 1e-10);
  }
}
/*****************************************************************/

/// @brief Verify that the inverse transform recovers the simple cos function
/// over x.
/// @details This test verifies the inverse transform applied to the assertion
/// of the one given for the forward FFT test
TEST(FFT, inverse_transform) {
  UInt N = 512;
  Matrix<complex> m(N);

  Real k = 2 * M_PI / N;
  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);

    if (i == 1 && j == 0) {
      val = N * N / 2;
    } else if (i == N - 1 && j == 0) {
      val = N * N / 2;
    } else {
      val = 0.0;
    }
  }

  Matrix<complex> res = FFT::itransform(m);

  for (auto&& entry : index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);

    ASSERT_NEAR(val.real(), cos(k * i), 1e-10);
  }
}
/*****************************************************************/

TEST(FFT, computeFrequencies) {
  int freq_i, freq_j;

  for (const int size : {3, 9, 27, 81}) {
    auto freqs = FFT::computeFrequencies(size);

    for (auto&& entry : index(freqs)) {
      int i = std::get<0>(entry);
      int j = std::get<1>(entry);
      auto& val = std::get<2>(entry);

      freq_i = i <= (size - 1) / 2 ? i : i - size;
      freq_j = j <= (size - 1) / 2 ? j : j - size;
      ASSERT_EQ(val.real(), freq_i);
      ASSERT_EQ(val.imag(), freq_j);
    }
  }
}

/*****************************************************************
 * Testing computeTemperature class
 *****************************************************************/

TEST(ComputeTemperature, homogeneousTemperature) {
  // Initialization of problem data
  constexpr Real SMALL_NUMBER = 1e-10;
  auto& factory = MaterialPointsFactory::getInstance();
  int N = 100;

  std::array<Real, 2> corner0 = {-1, -1};
  std::array<Real, 2> corner1 = {+1, +1};
  std::array<Real, 2> dx;
  dx[0] = abs(corner1[0] - corner0[0]) / N;
  dx[1] = abs(corner1[1] - corner0[1]) / N;

  // Homogeneous temperature field and zero heat flux
  // std::default_random_engine gen;
  // std::uniform_real_distribution<Real> dis(-10.0, 10.0);
  // Real temperature = dis(gen);
  // std::cout << "Testing for homogeneous temperature field = " << temperature;
  constexpr Real temperature = -5.;
  constexpr Real heatFlux = 0.;

  // Initializing system
  System system;
  for (int i = 0; i < N; i++) {
    for (int j = 0; j < N; j++) {
      auto particle = factory.createParticle();
      auto& materialParticle = static_cast<MaterialPoint&>(*particle);
      materialParticle.getTemperature() = temperature;
      materialParticle.getHeatRate() = heatFlux;

      auto& x = materialParticle.getPosition();
      x[0] = corner0[0] + dx[0] * Real(i);
      x[1] = corner0[1] + dx[1] * Real(j);
      x[2] = 0.;

      // system.addParticle(particle);
      system.addParticle(std::move(particle));
    }
  }

  // Initializing compute
  ComputeTemperature compute;
  compute.rho = 1.;       // Mass density
  compute.heatCap = 1.;   // Heat capacity
  compute.heatCond = 1.;  // Heat conductivity
  compute.deltaT = 1.;    // Time step

  // Let time advance a little and check that temperature did not change
  compute.compute(system);
  compute.compute(system);
  compute.compute(system);
  compute.compute(system);

  for (auto&& particle : system) {
    auto& materialParticle = static_cast<MaterialPoint&>(particle);
    // Temperature should not have changed
    ASSERT_NEAR(materialParticle.getTemperature(), temperature, SMALL_NUMBER);
  }
}
/*****************************************************************
 * Testing computeTemperature class
 *****************************************************************/

TEST(ComputeTemperature, sineWaveHeatSource) {
  // Initialization of problem data
  constexpr Real SMALL_NUMBER = 1e-10;
  auto& factory = MaterialPointsFactory::getInstance();
  const int N = 50;

  const std::array<Real, 2> corner0 = {-1, -1};
  const std::array<Real, 2> corner1 = {+1, +1};
  const Real L = abs(corner1[0] - corner0[0]);
  const Real _2Pi = 2. * M_PI;
  const std::array<Real, 2> dx = {abs(corner1[0] - corner0[0]) / N,
                                  abs(corner1[1] - corner0[1]) / N};

  // Sine wave heat source
  auto heatFlux = [&](const int x) -> Real {
    return sin(_2Pi * x / L) * pow(_2Pi / L, 2);
  };
  // Corresponding equilibrium temperature
  auto initialTemp = [&](const int x) -> Real { return sin(_2Pi * x / L); };

  // Initializing system
  System system;
  for (int i = 0; i < N; i++) {
    for (int j = 0; j < N; j++) {
      auto particle = factory.createParticle();
      auto& materialParticle = static_cast<MaterialPoint&>(*particle);

      auto& x = materialParticle.getPosition();
      x[0] = corner0[0] + dx[0] * Real(i);
      x[1] = corner0[1] + dx[1] * Real(j);
      x[2] = 0.;
      materialParticle.getTemperature() = initialTemp(x[0]);
      materialParticle.getHeatRate() = heatFlux(x[0]);

      system.addParticle(std::move(particle));
    }
  }

  // Initializing compute
  ComputeTemperature compute;
  compute.rho = 1.;       // Mass density
  compute.heatCap = 1.;   // Heat capacity
  compute.heatCond = 1.;  // Heat conductivity
  compute.deltaT = 1.;    // Time step

  // Let time advance a little and check that temperature did not change
  compute.compute(system);

  for (auto&& particle : system) {
    auto& materialParticle = static_cast<MaterialPoint&>(particle);
    const auto& x = materialParticle.getPosition();
    // Temperature should not have changed
    ASSERT_NEAR(materialParticle.getTemperature(), initialTemp(x[0]), SMALL_NUMBER);
  }
}
