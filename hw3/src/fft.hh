#ifndef FFT_HH
#define FFT_HH
/* ------------------------------------------------------ */
#include "matrix.hh"
#include "my_types.hh"
#include <fftw3.h>
/* ------------------------------------------------------ */

struct FFT {

  static Matrix<complex> transform(Matrix<complex>& m);
  static Matrix<complex> itransform(Matrix<complex>& m);
  static Matrix<std::complex<int>> computeFrequencies(int size);
};

/// @brief Forward FFT
inline Matrix<complex> FFT::transform(Matrix<complex>& m_in) {
  const int mSize = m_in.size();
  Matrix<complex> transformedMatrix(mSize);

  // fftw requires its own complex-type, but std::complex is passing the tests
  auto* mInData = reinterpret_cast<fftw_complex*>(m_in.data());
  auto* mOutData = reinterpret_cast<fftw_complex*>(transformedMatrix.data());

  fftw_plan plan = fftw_plan_dft_2d(mSize, mSize, mInData, mOutData,
                                    FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_execute(plan);
  fftw_destroy_plan(plan);

  return transformedMatrix;
}

/* ------------------------------------------------------ */

/// @brief Backward FFT
inline Matrix<complex> FFT::itransform(Matrix<complex>& m_in) {
  const int mSize = m_in.size();
  Matrix<complex> transformedMatrix(mSize);

  // fftw requires its own complex-type, but std::complex is passing the tests
  auto* mInData = reinterpret_cast<fftw_complex*>(m_in.data());
  auto* mOutData = reinterpret_cast<fftw_complex*>(transformedMatrix.data());

  fftw_plan plan = fftw_plan_dft_2d(mSize, mSize, mInData, mOutData,
                                    FFTW_BACKWARD, FFTW_ESTIMATE);
  fftw_execute(plan);
  fftw_destroy_plan(plan);

  // normalize
  transformedMatrix *= 1. / (mSize * mSize);

  return transformedMatrix;
}

/* ------------------------------------------------------ */

inline Matrix<std::complex<int>> FFT::computeFrequencies(int size) {
  Matrix<std::complex<int>> m_out(size);

  for (int i = 0; i < size; ++i) {
    for (int j = 0; j < size; ++j) {
      int freq_x = (i <= size / 2) ? i : i - size;
      int freq_y = (j <= size / 2) ? j : j - size;
      m_out(i, j) = std::complex<int>(freq_x, freq_y);
    }
  }

  return m_out;
}

#endif  // FFT_HH
