#ifndef __COMPUTE_TEMPERATURE__HH__
#define __COMPUTE_TEMPERATURE__HH__

/* -------------------------------------------------------------------------- */
#include "compute.hh"
#include "compute_interaction.hh"
#include <functional>
#include "matrix.hh"
#include "my_types.hh"

// Computing the interaction between particles

class ComputeTemperature : public Compute {

public:
  ComputeTemperature();

  virtual ~ComputeTemperature() {};
  
  void compute(System& system) override;

  Real rho; // Mass density
  Real heatCap; // Heat capacity
  Real heatCond; // Heat conductivity
  Real deltaT; // Time step
};

/* -------------------------------------------------------------------------- */
#endif  //__COMPUTE_TEMPERATURE__HH__
