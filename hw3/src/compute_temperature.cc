#include "compute_temperature.hh"
#include "fft.hh"
#include "material_point.hh"
#include <cmath>

/* -------------------------------------------------------------------------- */

ComputeTemperature::ComputeTemperature()
{
  this->rho = 1;
  this->heatCap = 1;
  this->heatCond = 1;
  this->deltaT = 0.001;
};

/* -------------------------------------------------------------------------- */

void ComputeTemperature::compute(System& system) {
  
  // Initialization of the matrices
  
  UInt n = system.getNbParticles();
  UInt N = std::sqrt(n);
  Matrix<complex> tempMatrix(N);
  Matrix<complex> heatMatrix(N);
  Matrix<complex> tempFourier(N);
  Matrix<complex> tempFourierDiff(N);
  Matrix<complex> heatFourier(N);
  Matrix<complex> tempFourierInv(N);
  
  // Constructing the coordinate matrix
  
  Matrix<std::complex<int>> CoordFourier = FFT::computeFrequencies(N);

  // Constructing the heat and temperature matrices
  
  for (UInt i = 0; i < N; i++) {
    for (UInt j = 0; j < N; j++) {
      auto& particle = system.getParticle(i * N + j);
      auto& mp = static_cast<MaterialPoint&>(particle);
      tempMatrix(i,j) = mp.getTemperature();
      heatMatrix(i,j) = mp.getHeatRate();
    }
  }

  // Taking the Fourier transform of the heat equation
  
  tempFourier = FFT::transform(tempMatrix);
  heatFourier = FFT::transform(heatMatrix);

  // Constructing the del_heat / del_t matrix
  
  Real eqConstant = (1.0 / (this->rho * this->heatCap));
  for (UInt i = 0; i < N; i++) {
    for (UInt j = 0; j < N; j++) {

      // Converting frequencies into rad/sec
      
      Real q_x = std::real(CoordFourier(i,j)) * 2.0 * M_PI;
      Real q_y = std::imag(CoordFourier(i,j)) * 2.0 * M_PI;
      Real coords = q_x * q_x + q_y * q_y;
      tempFourierDiff(i,j) = eqConstant * (heatFourier(i,j) - this->heatCond * tempFourier(i,j) * coords);
    }
  }

  // Inverse Fourier Transform

  tempFourierInv = FFT::itransform(tempFourierDiff);

  // Euler integration

  // Updating the heat and temperature matrices
  
  for (UInt i = 0; i < N; i++) {
    for (UInt j = 0; j < N; j++) {
      auto& particle = system.getParticle(i * N + j);
      
      auto& mp = static_cast<MaterialPoint&>(particle);
      
      // In boundary temperature should not change, should be zero
      
      if (mp.isBoundary()== false){
        mp.getTemperature() += this->deltaT * std::real(tempFourierInv(i,j));
      } else {
        mp.getTemperature() += 0.; 
      }
    }
  }

};

/* -------------------------------------------------------------------------- */
