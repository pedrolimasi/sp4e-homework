# Objective: Heat Equation Solver with FFTW

This project implements a solver for the transient heat equation in two dimensions using the Fastest Fourier Transform in the West (FFTW) library. The heat equation is given by:

$$
\rho C \frac{\partial \theta}{\partial t} - \kappa\left(\frac{\partial^2 \theta}{\partial x^2} + \frac{\partial^2 \theta}{\partial y^2}\right) = h_v
$$

where:
- $\rho$ is the mass density,
- $C$ is the specific heat capacity,
- $\kappa$ is the heat conductivity,
- $h_v$ is the volumetric heat source, and
- $\theta$ is the temperature.

# Work Division Strategy

## Responsibilities

**Pedro**: 
- Implementation of `Inverse Fourier Transform` and the corresponding testcase. 
- Implementation of `ComputeTemperature` subclass.
- Implementation of a validating test for an initial homogeneous temperature and no heat flux.
- Implementation of a validating test for a volumetric heat source.
 

**Antareep**: 
- Implementation of `Forward Fourier Transform`.
- Implementation of `Compute Frequencies` and its corresponding testcase.
- Implementation of `ComputeTemperature` subclass.
- Documentation of `Exercise 4.6`.

# Exercise 1

The additional classes in the project are described below:

**MaterialPoint**:

This class represents a particle in a material system. It inherits from the `Particle` class, which provides basic properties for particles, such as position, velocity, and mass. The `MaterialPoint` class adds additional properties specific to material points, such as heat rate and temperature. These properties are used to track the state of a material particle and to simulate its behavior over time.

**MaterialPointFactory**:

This class is responsible for creating instances of `MaterialPoint` objects. It inherits from the `ParticlesFactoryInterface` class, which provides a common interface for creating particles. The `MaterialPointFactory` class defines specific methods for creating `MaterialPoint` objects with specific properties. This allows for the creation of different types of particles, such as particles with different initial temperatures or velocities.

**Matrix**:

This class is a template class that provides methods for manipulating 2D data. It can be used to create, access, and modify matrices of different data types. This class is useful for storing and manipulating data that is organized in a 2D grid, such as the temperature or pressure field of a material system.

**FFT**:

This class is used to compute the 2D discrete Fourier Transform (DFT) of a complex matrix. The DFT is a mathematical operation that transforms a signal from the spatial domain to the frequency domain. This class is useful for analyzing the frequency content of a signal, which can be helpful for understanding the dynamics of a material system.

# Exercise 4.6

To launch a simulation that produces dumps observable with ParaView for a grid of 512 × 512 particles, the user needs to follow these general steps:

**Configure the Simulation Parameters**:

Define the physical parameters of your simulation, such as mass density (rho), heat capacity (heatCap), heat conductivity (heatCond), and the time step (deltaT) in the ComputeTemperature class.

**Initialize the Particle Grid**:

Use the MaterialPointsFactory to create a grid of material points with a size of 512 × 512.
Set the initial temperature and heat flux for each particle in the grid.
Initialize the System:

**Initializing Particle Factory**:

Depending on the specified particle_type, it initializes the corresponding particle factory. The possible types are "planet," "ping_pong," or "material_point."

**Initialize the ComputeTemperature Class**:

Create an instance of the ComputeTemperature class. Set the relevant simulation parameters (rho, heatCap, heatCond, deltaT) if they are not set in the class constructor.

**Creating Particle System**:

The particle system is then created using the initialized factory (`ParticlesFactoryInterface`) to create a simulation (`SystemEvolution`) based on the specified input file and time step.

**Run the Simulation Loop**:

Implement a simulation loop where you repeatedly call the compute method of the ComputeTemperature class to advance the simulation. After each time step, you may save the state of the system or relevant data for visualization. The main script calls the `evolve()` method of the `SystemEvolution` object to perform the particle system simulation.

**Generate Dumps for ParaView**:

During the simulation loop, periodically save the state of the system in a format that ParaView can read. Common formats include VTK (Visualization Toolkit) files. ParaView supports various data formats, including structured grids, unstructured grids, and point clouds. Choose a format that suits your simulation data.

**Visualize Results in ParaView**:

After the simulation run, open ParaView.
Load the saved data files using the "File" > "Open" menu.
Choose the appropriate reader for the file format you used to save the simulation data.
Explore and visualize the results using ParaView's powerful visualization tools.





