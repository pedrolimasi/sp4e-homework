import numpy as np
from numpy.typing import ArrayLike
import typing


def gmres(
    A: ArrayLike,
    b: ArrayLike,
    x0: ArrayLike = None,
    maxIter: int = 100,
    tol: float = 1e-6,
    keepHistory: bool = False,
    iterations: list = []
) -> ArrayLike:
    """Computes solution to linear system using iterative solver following the Generalized Minimal Residual method

    Args:
        A (ArrayLike): Square matrix of coefficients
        b (ArrayLike): System's right-hand-side vector
        x0 (ArrayLike, optional): Initial guess. Defaults to numpy.zeros_like(b).
        maxIter (int, optional): Upper limit on number of iterations. Defaults to 100.
        tol (float, optional): Tolerance to define solution convergence. Defaults to 1e-6.
        keepHistory (bool, optional): Flag for saving and exporting approximation steps. Defaults to False.

    Returns:
        OptimizeResult: Minimal class inspired by numpy.OptimizeResult.
    """

    # Consistency
    x0 = np.zeros_like(b) if x0 is None else x0
    spaceDimension = len(b)
    assert (
        len(A) == spaceDimension
    ), "Matrix A and vector b must have compatible dimensions."
    assert len(A[0]) == spaceDimension, "Matrix A must be square."
    assert (
        len(x0) == spaceDimension
    ), "Initial guess and vector b must have compatible dimensions."
    # In the worst case, GMRES converges at the spaceDimension-th iteration. (c.f. wikipedia section on convergence)
    maxIter = min(maxIter, spaceDimension)

    if keepHistory:
        iterations.append(x0)

    # Local datastructure
    H  = np.zeros((maxIter + 1, maxIter))
    Q  = np.zeros((spaceDimension, maxIter + 1))
    residual = b - np.einsum("ij,j", A, x0)
    residualNorm = np.linalg.norm(residual)
    e1 = np.zeros(maxIter + 1)
    e1[0] = 1
    e1Beta = e1*residualNorm
    Q[:, 0] = residual / residualNorm

    for nit in range(0, maxIter):
        if residualNorm < tol:
            break

        # Arnoldi iteration
        q = np.einsum("ij,j", A, Q[:, nit])
        for j in range(nit + 1):
            H[j, nit] = np.einsum("i,i", q, Q[:, j])
            q -= H[j, nit] * Q[:, j]

        H[nit + 1, nit] = np.linalg.norm(q)
        Q[:, nit + 1] = q / H[nit + 1, nit]

        # Solve least squares problem
        y = np.linalg.lstsq(H, e1Beta, rcond=None)[0]
        x = x0 + np.einsum("ij,j", Q[:,:-1], y)
        if keepHistory:
            iterations.append(x)

        residual = b - np.einsum("ij,j", A, x)
        residualNorm = np.linalg.norm(residual)

    return x


def getRandInvertibleMatrix(size):
    while True:
        matrix = np.random.rand(size, size)
        if np.linalg.det(matrix) != 0:
            return matrix
    

def stressTesting(nTests, systemSize, tol):
    for s in range(nTests):
        A = getRandInvertibleMatrix(systemSize)
        b = np.random.rand(systemSize)
        actualSolution = np.linalg.solve(A,b)
        solution = gmres(A, b)
        assert all(map(lambda diff: diff<tol, solution - actualSolution))
