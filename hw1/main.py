import argparse
import numpy as np
import GMRES
import scipy.optimize
import scipy.sparse.linalg
import optimizer


parser = argparse.ArgumentParser()
parser.add_argument(
    "-A",
    "--matrixpath",
    type=str,
    help="Path to matrix stored in csv file",
    default="hw1/matrixA.csv",
)
parser.add_argument(
    "-b",
    "--rhspath",
    type=str,
    help="Path to rhs vector stored in csv file",
    default="hw1/rhs.csv",
)
parser.add_argument(
    "-a",
    "--algorithm",
    choices=["GMRES", "BFGS", "LGMRES"],
    help="Choose algorithm",
    type=str,
    default="GMRES",
)
parser.add_argument(
    "-p",
    "--plot",
    help="Plot flag to generate plot of each iteration",
    type=int,
    default=1,
)
args = parser.parse_args()


A = np.genfromtxt(args.matrixpath, delimiter=",", dtype=float)
b = np.genfromtxt(args.rhspath, delimiter=",", dtype=float)


# Calling the minimizer to find the solution
solution = optimizer.getMinimizer(
    A, b, 10 * np.ones(len(b)), method=args.algorithm, plot=True
)

# Printing the solution
if solution is not None:
    print("Solution is:", solution)
else:
    print("Optimization did not converge.")


print(f"Your lhs matrix is \n{A}")
print(f"Your rhs vector is \n{b}")
print(f"You chose method {args.algorithm}")
print(f"Which gives the solution {solution}")
print(f"Solution should be {np.linalg.solve(A,b)}")
