import matplotlib.pyplot as plt
import numpy as np
from numpy.typing import ArrayLike
from typing import Callable


def plotMinimizationSteps(obj_func: Callable, iterations: ArrayLike):
    assert (
        len(iterations[0]) == 2
    ), "This plot function is restricted to scalar functions of 2D vectors"

    # Create a surface plot of the objective function and the iterative path
    x_range = np.linspace(-10, 10, 100)
    y_range = np.linspace(-10, 10, 100)
    X, Y = np.meshgrid(x_range, y_range)
    Z = np.asarray(
        [
            [obj_func((X[i, j], Y[i, j])) for j in range(len(X))]
            for i in range(len(X[0]))
        ]
    )

    fig = plt.figure(figsize=(12, 6))
    ax = fig.add_subplot(111, projection="3d")
    ax.plot_surface(X, Y, Z, cmap="viridis", alpha=0.8)
    ax.scatter(
        [s[0] for s in iterations],
        [s[1] for s in iterations],
        [obj_func(s) for s in iterations],
        color="red",
        s=50,
    )
    ax.plot(
        [s[0] for s in iterations],
        [s[1] for s in iterations],
        [obj_func(s) for s in iterations],
        color="red",
    )

    # Annotate plot
    ax.set_xlabel("X")
    ax.set_ylabel("Y")
    ax.set_zlabel("Objective Function Value")
    ax.set_title("Objective Function and Iterative Path")
    ax.tricontour(X.reshape(-1), Y.reshape(-1), Z.reshape(-1))

    plt.show()
