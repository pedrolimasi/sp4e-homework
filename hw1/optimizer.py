import numpy as np
from numpy.typing import ArrayLike
from scipy.optimize import minimize
from scipy.sparse.linalg import lgmres
import plotter
import GMRES


def getMinimizer(
    A: ArrayLike,
    b: ArrayLike,
    x0: ArrayLike = None,
    tol: float = 1e-6,
    method: str = "GMRES",
    plot: bool = True,
) -> ArrayLike:
    """Computes argmin to S(x) = x^T A x/2 - x^T b.

    Args:
        A (ArrayLike): Square matrix coefficients.
        b (ArrayLike): System's right-hand-side vector.
        x0 (ArrayLike, optional): Initial guess. Defaults to numpy.zeros_like(b).
        method (str, optional): Minimization method. Defaults to "GMRES".
        plot (bool, optional): Plot iterations of minimization algorithm. Defaults to True.

    Returns:
        ArrayLike: Point where function attains its minimum.
    """
    # Consistency
    x0 = np.zeros_like(b) if x0 is None else x0
    spaceDimension = len(b)
    assert (
        len(A) == spaceDimension
    ), "Matrix A and vector b must have compatible dimensions."
    assert len(A[0]) == spaceDimension, "Matrix A must be square."
    assert (
        len(x0) == spaceDimension
    ), "Initial guess and vector b must have compatible dimensions."

    obj_func = lambda x: 0.5 * np.dot(x, np.dot(A, x)) - np.dot(b, x)

    iterations = []  # To store solutions at each iteration

    def record_solution(x):
        return iterations.append(x.copy())

    if method == "LGMRES":
        result = lgmres(A, b, x0=x0, callback=record_solution, tol=tol)
        result = result[0]
    elif method == "BFGS":
        result = minimize(
            obj_func, x0, method=method, callback=record_solution, tol=tol
        )
        if result.success:
            result = result.x
        else:
            result = None
    elif method == "GMRES":
        result = GMRES.gmres(
            A, b, x0, len(b), tol=tol, keepHistory=plot, iterations=iterations
        )

    if plot:
        plotter.plotMinimizationSteps(obj_func, iterations)

    return result
