# Homework 1

The code provided here implements three different approaches to minimize a functional of the form

$$
S(\mathbf{x}) = \dfrac{1}{2}\mathbf{x}\cdot\mathbf{A}\mathbf{x} -\mathbf{x}\cdot\mathbf{b}
$$

The minimizers available are

- GMRES: Generalized Minimal Residual.
- LGMRES: Left-preconditioned Generalized Minimal Residual.
- BFGS: Broyden-Fletcher-Goldfarb-Shanno.

We provide a command line interface `main.py` and have wrapped the minimizers so that needed input for all methods are the same: The matrix A and the vector b.

# Installing

This is a pure python, pipenv project. You can build it by installing pipenv first, then using it to build the project. Assuming you have a Python distribution and `pip` available in your `PATH`, the following commands will build a local virtual environment under `hw1/.venv`.
On a terminal, execute:

```shell
pip install pipenv
pipenv install
pipenv shell
```

Assumed versions for dependencies are listed in the provided `Pipfile`.

# CLI

You may request help from the command line by calling `python main.py -h`. The CLI arguments available are:

- `-A`, or `--matrixpath` followed by  `<path-to-matrix-csv-file>`
- `-b`, or `--rhspath` followed by  `<path-to-rhs-csv-file>`
- `-a`, or `--algorithm` followed by  `<algorithm-name>` choose between ("GMRES", "BFGS", "LGMRES")
- `-p`, or `--plot` (standalone argument)

### Example

```shell
python main.py -A "matrixA.csv" -b "rhs.csv" --algorithm GMRES --plot
```

# Using as modules

You may use `main.py` as a template to use `optimizer.py` as a module.
