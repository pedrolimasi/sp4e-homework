# Objective: Pybind and trajectory optimisation

This subproject uses the external library Pybind11 to create Python bindings of the C++ Particles’ code. This is a solution to homework 4.

# Dependencies

- CMake
- python3.10
- pybind11
- eigen
- googletest (optional for the examples solved here)
- FFTW (optional for the examples solved here)

Eigen, pybind11 and googletest are available as submodules, which you may initialize through

```shell
git submodule update --init
```

# Build

To build this code, make sure you have the dependencies accessible in your environment, then:

```bash
cd hw4
mkdir build
cd build
cmake ..
make
```

Pybind11 is configured to build the binding module to `build/src/`. Make sure that path is available in your environment so you can import the bindings through

```python
import pypart
```

If you have `pipenv` (installable through `pip3 install pipenv`), this homework includes a `Pipfile` and a `.env` file to help you setup virtual environment.
Just modify the `$PYTHONPATH` variable in  the `.env` file to include the full path to `sp4e-homework/hw4/build/src/`, where the bindings module should have been compiled to.

To create and source a virtual environment with `pipenv`:
```shell
cd hw4
pipenv install
pipenv shell
```

`pipenv` will auto detect `.env` and use its variables during its process.

# Usage

This homework includes a python module `main.py` which may be used from the command line to compute an approximation for a particle system.
You can get help to its usage through `python src/main.py --help`, which will output:
```plain
usage: main.py [-h] nsteps freq filename particle_type timestep

Particles code

positional arguments:
  nsteps         specify the number of steps to perform
  freq           specify the frequency for dumps
  filename       start/input filename
  particle_type  particle type
  timestep       timestep

options:
  -h, --help     show this help message and exit
```

The files `exercise5` and `exercise6` contain solutions to the respective exercises. They're also intended as examples on how to use the pure-python implementations: 
- `readPositions`
- `computeError`
- `generateInput`
- `runAndComputeError`