import os
import numpy as np
from itertools import islice

particleNameList = [
    "sun",
    "mercury",
    "neptune",
    "jupiter",
    "uranus",
    "mars",
    "earth",
    "venus",
    "saturn",
]


def readSinglePosition(planetIndex: int, filePath: str):
    with open(filePath, "r") as file:
        targetLine = next(islice(file, planetIndex, planetIndex + 1), None)
        position = map(float, targetLine.strip().split(" ")[:3])
        return list(position)


def readPositions(planetName: str, directory: str):
    fileNames = filter(lambda s: s.endswith(".csv"), sorted(os.listdir(directory)))
    planetRow = 1 + particleNameList.index(planetName)
    positions = np.fromiter(
        map(
            lambda stepFile: readSinglePosition(
                planetRow, os.path.join(directory, stepFile)
            ),
            fileNames,
        ),
        dtype=(float, 3),
    )
    return positions


def computeError(positions: np.ndarray, measurements: np.ndarray):
    nPoints = len(positions)
    squaredError = (measurements[:nPoints] - positions) ** 2
    L2errorNorm = np.sqrt(np.sum(squaredError))
    return L2errorNorm


if __name__ == "__main__":
    measurements = readPositions("mercury", "src/trajectories")
    positions = readPositions("mercury", "dumps")
    errorNorm = computeError(positions, measurements)
    # assert errorNorm < 1e-10
    pass
