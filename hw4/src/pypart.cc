#include <pybind11/pybind11.h>

namespace py = pybind11;

#include "csv_writer.hh"
#include "material_points_factory.hh"
#include "ping_pong_balls_factory.hh"
#include "planets_factory.hh"

#include "compute.hh"
#include "compute_gravity.hh"
#include "compute_interaction.hh"
#include "compute_temperature.hh"
#include "compute_verlet_integration.hh"

PYBIND11_MODULE(pypart, m) {

  m.doc() = "pybind of the Particles project";

  // bind the routines here

  // Particle factories
  py::class_<ParticlesFactoryInterface>(m, "ParticlesFactoryInterface")
      .def("getInstance", &ParticlesFactoryInterface::getInstance,
           py::return_value_policy::reference)
      .def("createSimulation",
           py::overload_cast<const std::string&, Real, py::function>(
               &ParticlesFactoryInterface::createSimulation<py::function>),
           py::return_value_policy::reference)
      /** Exercise 1.2: This overload of createSimulation takes a functor to
         define the createComputes property of the instantiated factory. It
         enables a user to define their own create computes so not to depend on
         the default behaviour.**/
      .def_property("system_evolution",
                    &ParticlesFactoryInterface::getSystemEvolution, nullptr);

  py::class_<MaterialPointsFactory, ParticlesFactoryInterface>(
      m, "MaterialPointsFactory")
      .def("getInstance", &MaterialPointsFactory::getInstance,
           py::return_value_policy::reference);

  py::class_<PlanetsFactory, ParticlesFactoryInterface>(m, "PlanetsFactory")
      .def("getInstance", &PlanetsFactory::getInstance,
           py::return_value_policy::reference);

  py::class_<PingPongBallsFactory, ParticlesFactoryInterface>(
      m, "PingPongBallsFactory")
      .def("getInstance", &PingPongBallsFactory::getInstance,
           py::return_value_policy::reference);

  // Compute
  py::class_<Compute, std::shared_ptr<Compute>>(m, "Compute",
                                                py::dynamic_attr());

  // Compute interaction
  py::class_<ComputeInteraction, Compute, std::shared_ptr<ComputeInteraction>>(
      m, "ComputeInteraction", py::dynamic_attr());

  // Compute gravity
  py::class_<ComputeGravity, ComputeInteraction,
             std::shared_ptr<ComputeGravity>>(m, "ComputeGravity",
                                              py::dynamic_attr())
      .def(py::init<>())
      .def("setG", &ComputeGravity::setG, py::arg("G"));

  // Compute temperature
  py::class_<ComputeTemperature, Compute, std::shared_ptr<ComputeTemperature>>(
      m, "ComputeTemperature", py::dynamic_attr())
      .def(py::init<>())

      // setting read/write access to private members of ComputeTemperature

      .def_property(
          "conductivity", &ComputeTemperature::getConductivity,
          [](ComputeTemperature& c, Real val) { c.getConductivity() = val; })

      .def_property("L", &ComputeTemperature::getL,
                    [](ComputeTemperature& c, Real val) { c.getL() = val; })

      .def_property(
          "capacity", &ComputeTemperature::getCapacity,
          [](ComputeTemperature& c, Real val) { c.getCapacity() = val; })

      .def_property(
          "deltat", &ComputeTemperature::getDeltat,
          [](ComputeTemperature& c, Real val) { c.getDeltat() = val; })

      .def_property(
          "density", &ComputeTemperature::getDensity,
          [](ComputeTemperature& c, Real val) { c.getDensity() = val; });

  // Compute verlet interaction
  py::class_<ComputeVerletIntegration, Compute,
             std::shared_ptr<ComputeVerletIntegration>>(
      m, "ComputeVerletIntegration", py::dynamic_attr())
      .def(py::init<Real>())
      .def("addInteraction", &ComputeVerletIntegration::addInteraction);

  // System
  py::class_<System, std::shared_ptr<System>>(m, "System", py::dynamic_attr())
      .def(py::init<>());

  // System evolution
  py::class_<SystemEvolution>(m, "SystemEvolution", py::dynamic_attr())
      .def("addCompute", &SystemEvolution::addCompute, py::arg("compute"))
      .def("evolve", &SystemEvolution::evolve)
      .def("getSystem", &SystemEvolution::getSystem)
      .def("setNSteps", &SystemEvolution::setNSteps, py::arg("nsteps"))
      .def("setDumpFreq", &SystemEvolution::setDumpFreq, py::arg("freq"));

  // CSV writer
  py::class_<CsvWriter>(m, "CsvWriter")
      .def(py::init<const std::string&>())
      .def("write", &CsvWriter::write);
}
