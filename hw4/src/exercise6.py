import os
import csv
import numpy as np
import pandas as pd

import main as hw4
from exercise5 import computeError, readPositions


def generateInput(
    velocity_scale: float,
    target_planet: str,
    input_file_path: str,
    output_file_path: str,
) -> None:
    df = pd.read_csv(input_file_path, index_col=10, sep=" ")
    df.loc[target_planet]["VX"] *= velocity_scale
    df.loc[target_planet]["VY"] *= velocity_scale
    df.loc[target_planet]["VZ"] *= velocity_scale
    df["name"] = df.index
    df.to_csv(output_file_path, index=False, sep=" ")


def launchParticles(input: str, nb_steps: int, freq: int, timestep: float):
    hw4.main(nb_steps, freq, input, "planet", timestep)


def runAndComputeError(
    scale: float,
    planet_name: str,
    input: str,
    nb_steps: int,
    freq: int,
    timestep: float,
):
    referenceTrajectory = readPositions(planet_name, "src/trajectories")[::freq]
    generateInput(scale, planet_name, input, "scaledInput.csv")
    launchParticles("scaledInput.csv", nb_steps, freq, timestep)
    computedTrajectory = readPositions(planet_name, "dumps")
    assert len(referenceTrajectory) >= len(
        computedTrajectory
    ), "Incompatible number of trajectory points. Did you remember to clean the dumps directory before trying to analyze error?"
    errorNorm = computeError(computedTrajectory, referenceTrajectory)

    return errorNorm


if __name__ == "__main__":
    if not os.path.exists("dumps"):
        os.makedirs("dumps")
    err = runAndComputeError(1, "mercury", "src/init.csv", 365, 1, 1)
    pass
