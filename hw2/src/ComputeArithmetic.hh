#ifndef COMPUTEARITHMETIC_HPP
#define COMPUTEARITHMETIC_HPP

#include "Series.hh"

class ComputeArithmetic : public Series {
  public:
    real compute(uint N) override;
    real term(uint N) override;

    real getAnalyticPrediction(uint N) const override;
};
#endif