#include "WriteSeries.hh"
#include "DumperSeries.hh"
#include <cmath>
#include <fstream>
#include <iostream>
#include <ostream>
#include <stdexcept>

void WriteSeries::dump(std::ostream &os) {
    os.precision(this->precision);

    os << "iter" << this->separator << "value" << std::endl;
    for (int i = 0; i < this->maxiter; ++i) {
        os << i + 1 << this->separator << this->series.compute(i + 1)
           << std::endl;
    }

    real analytic = this->series.getAnalyticPrediction(this->maxiter);
    os << "analytic" << this->separator << analytic << std::endl;
}

void WriteSeries::dump(std::string const &filename,
                       std::string const extension) {
    this->setSeparator(extension);
    std::string new_filename = filename;
    new_filename.append(this->file_extension);
    std::ofstream outfile(new_filename);

    dump(outfile);

    outfile.close();
}

void WriteSeries::setSeparator(std::string const extension) {
    if (extension == "txt") {
        this->separator = ' ';
    } else if (extension == "csv") {
        this->separator = ',';
    } else if (extension == "psv") {
        this->separator = '|';
    } else {
        throw std::runtime_error("Error: separator should be ' ', ',' or '|'.");
    }
}