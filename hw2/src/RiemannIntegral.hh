#include "Series.hh"
#include <cmath>
#include <iostream>

/// @brief Available functions enum
enum class Efunction { x3, cos, sin };

/// @brief A class to compute the Riemann integral of a scalar function
/// @details Currently available functions are x^3, cos(x) and sin(x)
class RiemannIntegral : public Series {
  protected:
    Efunction function = Efunction::x3; /// @brief Function enumeration
    real lowerBound = -1; /// @brief Lower bound of integration
    real upperBound = +1; /// @brief Upper bound of integration
    real dx = -1; /// @brief Integration step

  public:
    real compute(uint N) override;
    real term(uint N) override;
    // real getAnalyticPrediction(uint N) const override;
    /// @brief Basic constructor
    /// @param func 
    /// @param lowerBound 
    /// @param upperBound 
    RiemannIntegral(Efunction func, real lowerBound, real upperBound)
        : function(func), lowerBound(lowerBound), upperBound(upperBound){};
};
