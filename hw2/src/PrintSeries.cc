#include "PrintSeries.hh"
#include "Series.hh"
#include "DumperSeries.hh"
#include <iostream>

// PrintSeries::PrintSeries(Series& s, int frequency, int maxiter)
    // : DumperSeries(s), frequency(frequency), maxiter(maxiter) {

void PrintSeries::dump(std::ostream &os) {

    for (int i = 1; i <= maxiter; i++) {
        if (i % frequency == 0) {
            double analyticPrediction = series.getAnalyticPrediction(i);
            double result = series.compute(i);
            double convergence = std::abs(analyticPrediction - result);
            os << "Taking N=" << i << ": " << result << " (Convergence is: " << 100 - convergence << " to Analytical value:" << analyticPrediction << ")" << std::endl;
        }
    }
}
