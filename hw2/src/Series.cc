#include "Series.hh"
#include <algorithm>
#include <iostream>
#include <numeric>
#include <ranges>

real Series::accumulate(uint N, std::function<real(real, uint)> accumulator) {
    // Compute series
    auto sequenceRange = std::views::iota(this->getCurrentIndex(), N);
    auto result = accumulator(this->current_value, N) +
                  std::accumulate(sequenceRange.begin(), sequenceRange.end(), 0,
                                  accumulator);

    // Update current state and return
    this->current_value = result;
    this->current_index = N;

    return result;
}

real Series::compute(uint N) {
    if (this->current_index > N) {
        this->current_value = 0;
        this->current_index = 1;
    }

    while (this->current_index <= N) {
        this->current_value += term(this->current_index);
        this->current_index++;
    }
    return this->current_value;
}