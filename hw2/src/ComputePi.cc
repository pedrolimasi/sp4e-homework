#include "ComputePi.hh"
#include <cmath>

ComputePi::ComputePi() {}

// real Accumulator(real current, uint index){ return (current + 1/(index*index));}

// real ComputePi::compute(uint N) {
//     return Series::accumulate(N, Accumulator);
//     // return std::sqrt(6* Series::accumulate(N, Accumulator));
// }


// ComputePi::ComputePi() {}

double ComputePi::term(uint N) {return 6/(N*N);}


double ComputePi::compute(uint N) {
    double sum = 0.0;
    for (uint k = 1; k <= N; ++k) {
        sum += 1.0 / (k * k);
    }
    return std::sqrt(6.0 * sum);
}
double ComputePi::getAnalyticPrediction(uint N) const {
    // Implement the analytic prediction for Pi
    return 3.14159265358979323846; // Replace with the actual analytic prediction
}

