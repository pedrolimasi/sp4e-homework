#ifndef SERIES_HPP
#define SERIES_HPP

#include <cstdint>
#include <limits>
#include <functional>

using uint = uint_least32_t;
using real = double;

class Series {
  protected:
    uint current_index = 1;
    real current_value = 0;

  public:
    virtual real compute(uint N) = 0;
    virtual real term(uint N) = 0;
    real accumulate(uint N, std::function<real(real,uint)> accumulator);
    
    uint getCurrentIndex() const { return this->current_index; }
    real getCurrentValue() const { return this->current_value; }

    static auto nan() { return std::numeric_limits<double>::quiet_NaN(); }
    virtual real getAnalyticPrediction(uint N) const {return nan();}
};

#endif