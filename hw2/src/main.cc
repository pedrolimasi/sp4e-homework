#include "ComputeArithmetic.hh"
#include "ComputeArithmetic.hh"
#include "ComputePi.hh"
#include "DumperSeries.hh"
#include "PrintSeries.hh"
#include "WriteSeries.hh"
#include "Series.hh"
#include <iostream>
#include <memory> // Include the <memory> header for smart pointers

int main() {
    // Prompting the user for the number of terms in the series
    int N;
    std::cout << "Enter the number of terms in the series N = ";
    std::cin >> N;

    std::unique_ptr<Series> seriesPtr = nullptr;
    // Prompt user for the type of series to compute
    char seriesType;
    std::cout << "Choose series type: ";
    std::cin >> seriesType;
    switch (seriesType) {
    case 'A':
        seriesPtr = std::make_unique<ComputeArithmetic>();
        break;
    case 'P':
        seriesPtr = std::make_unique<ComputePi>();
        break;
    case 'R':
    default:
        std::cout << "\nBad inpute. Expected usage:\n"
                  << "A - For arithmetic series\n"
                  << "P - For pi series\n"
                  << "R - For RiemannIntegral\n";
        return EXIT_FAILURE;
    }
    // double result = seriesPtr->compute(N);
    // Prompting the user for the output method (screen or file)
    char outputMethod;
    std::cout << "Type 'P' to print the result on the screen or 'W' to write "
                 "to a file: ";
    std::cin >> outputMethod;

    switch (outputMethod) {
    case 'P':{
        // Create an instance of PrintSeries with a frequency of 5 and maxiter
        // of 20
        PrintSeries printer(*seriesPtr, 1, N);
        // Call the dump method to output information
        printer.dump(std::cout);
        break;
    }
    case 'W':{
        // Writing the result to a file
        std::string filename;
        std::cout << "Enter the filename to write the result: ";
        std::cin >> filename;
        std::string extension = (filename.find_last_of(".") != std::string::npos) ? filename.substr(filename.find_last_of(".") + 1) : "csv";

        // Create a WriteSeries object to write the result to a file
        WriteSeries writer(*seriesPtr, N);

        writer.dump(filename, extension);
        break;
    }
    default:
        std::cout << "Invalid selection for the output method. Exiting program."
                  << std::endl;
    }

    return EXIT_SUCCESS;
}
