#include "RiemannIntegral.hh"
#include "Series.hh"
#include <functional>
#include <iostream>
#include <numeric>
#include <ranges>
#include <cmath>



real RiemannIntegral::compute(uint N) {
    this->dx = (this->upperBound - this->lowerBound) / N;
    return Series::compute(N);
}

real RiemannIntegral::term(uint i) {
    real a = this->lowerBound;
    real x = a + i * dx;
    switch (this->function) {
    case Efunction::x3:
        return std::pow(x, 3);
    case Efunction::cos:
        return std::cos(x);
    case Efunction::sin:
        return std::sin(x);
    };
    throw;
    return 0;
}
