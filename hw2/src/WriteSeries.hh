#pragma once

#include "DumperSeries.hh"

#include <cmath>
#include <fstream>
#include <ostream>
#include <array> 

enum class dumpformat{
  txt = 0,
  csv = 1,
  psv = 2
};

constexpr std::array<char, 3> separators = {' ', ',', '|'};

class WriteSeries : public DumperSeries {
  private:
    std::string file_extension;
    char separator;
  
  public:
    WriteSeries(Series &series, uint const maxiter)
      : DumperSeries(series, maxiter){};

    void dump(std::ostream &os) override;

    void dump(std::string const &filename, std::string const extension);

    void setSeparator(std::string const separator);
};
