#ifndef PRINT_SERIES_HPP
#define PRINT_SERIES_HPP

#include "DumperSeries.hh" // Include the base class header

class PrintSeries : public DumperSeries {
  public:
    PrintSeries(Series &s, int frequency, int maxiter)
        : DumperSeries(s, maxiter), frequency(frequency), maxiter(maxiter){};
    void dump(std::ostream &os) override;

  private:
    int frequency;
    int maxiter;
};

#endif
