// BEGIN: a1b2c3d4e5f6
#pragma once

#include "Series.hh"
#include <ostream>

class DumperSeries {
  protected:
    Series &series;
    uint precision = 6;
    uint maxiter = 100;

  public:
    DumperSeries(Series& series, uint maxiter = 100): series(series), maxiter(maxiter){};
    virtual void dump(std::ostream &os) = 0;
};
