#ifndef COMPUTE_PI_HPP
#define COMPUTE_PI_HPP

#include "Series.hh"

class ComputePi : public Series {
public:
    ComputePi();
    real compute(uint N) override;
    real term(uint N) override;
    real getAnalyticPrediction(uint N) const override;
};

#endif
