#include "ComputeArithmetic.hh"
#include "Series.hh"
#include <iostream>
#include <numeric>
#include <ranges>

real positiveAccumulator(real current, uint index) { return +index; }
real negativeAccumulator(real current, uint index) { return -index; }

real ComputeArithmetic::compute(uint N) {
    real summation = Series::compute(N);
    return summation;
}

real ComputeArithmetic::term(uint N) { return N; }
real ComputeArithmetic::getAnalyticPrediction(uint N) const {
    return N * (N + 1) / 2;
}