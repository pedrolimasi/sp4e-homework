import pandas as pd
import matplotlib.pyplot as plt
import os

def read_and_plot_data(file_path):
    # Extract file extension
    _, file_extension = os.path.splitext(file_path)

    if file_extension == ".txt":
        separator = ' '  # Space separator
    elif file_extension == ".csv":
        separator = ','  # Comma separator
    elif file_extension == ".psv":
        separator = '|'  # Pipe separator
    else:
        raise ValueError("Unsupported file extension")

    # Read the data from the file into a DataFrame
    data = pd.read_csv(file_path, delimiter=separator)

    # Extract the 'iter' and 'value' columns
    iterations = data['iter']
    values = data['value']

    # Create a plot
    plt.figure(figsize=(10, 6))
    plt.plot(iterations, values, label='Series Sum', marker='o')
    plt.xlabel('Iteration')
    plt.ylabel('Value')
    plt.title('Series Sum vs. No of Terms')
    plt.grid()
    plt.legend()
    plt.show()

if __name__ == "__main__":
    file_path = 'your_file.txt'  # Replace with the actual path to your file
    read_and_plot_data(file_path)
