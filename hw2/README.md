# Work Division Strategy

In this collaborative project, we intend to make use of the separation between declaration and implementation featured in C++ to organize the split of workload amongst developers.

## Roles and Responsibilities

1. **Architect**: The architect should write the abstract class and define the general behaviour of a derived class through declaration of datastructure and virtual methods. The work of this role focuses on header (`.hh`) files.

2. **Developer**: Developers are responsible for writing and maintaining the actual implementation of derived classes. They also fix bugs, and optimize the code and write documentation.

## Task Allocation

- **Pedro**: Architect for the `Series` class and developer for `ComputeArithmetic`, `WriteSeries`, and `RiemannIntegral`.
- **Antareep**: Architect for the `DumperSeries` class and developer for `ComputePi`, `PrintSeries`.

## Series Computation \& Analysis:
The src folder includes the following classes:

- **Series**: An abstract class representing series with methods for computation and analytic predictions.

```cpp
class Series{
public:
virtual double compute(unsigned int N) = 0;
}
```
- **ComputeArithmetic** : A derived class for computing arithmetic series.
```cpp
// Creating an instance of ComputeArithmetic
ComputeArithmetic arithmeticSeries;

// Computing the arithmetic sum for N = 10
double result = arithmeticSeries.compute(10);

// Displaying the result
std::cout << "Arithmetic sum: " << result << std::endl; 
```
- **ComputePi**: A derived class for approximating the mathematical constant Pi (π).
- **DumperSeries**: An abstract class for data dumping with methods for outputting series data.
```cpp
class DumperSeries{
public:
virtual void dump() = 0;
protected:
Series & series;
}
```
- **PrintSeries**: A class for printing series data to the console> the maximum value of Number of terms (N) and frequency can be specified by the user.
- **WriteSeries**: A class for writing series data to files with support for different file formats and separators.
```cpp
// Creating an instance of WriteSeries
WriteSeries writer(arithmeticSeries, frequency = 5, "txt");

// Writing the series data to a file
writer.dump("output", "txt");
```

The Python script "**read_and_plot**" allows the user to read series data from files with automatic file format and separator detection and create data visualizations. The file path needs to be specified by the user.

